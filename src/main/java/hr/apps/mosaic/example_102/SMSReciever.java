package hr.apps.mosaic.example_102;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by Stray on 1.10.2017..
 */

public class SMSReciever extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle b = intent.getExtras();
			SmsMessage[] msgs = null;
			String SMS = "";
			if (b != null){
				Object pdus[] = (Object[]) b.get("pdus");
				msgs = new SmsMessage[pdus.length];
				for (int i=0;i<pdus.length;i++){
					msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
					SMS += msgs[i].getMessageBody().toString();
				}
				Toast.makeText(context, SMS, Toast.LENGTH_SHORT).show();
			}
		}
}
