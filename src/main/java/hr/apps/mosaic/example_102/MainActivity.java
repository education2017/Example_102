package hr.apps.mosaic.example_102;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {
	private static final String PHONE_NUMBER = "09...";

	@BindView(R.id.etMyMessage) EditText etMessage;
	@BindView(R.id.bSendBroadcast) Button bSendBroadcast;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
	}

	@OnClick(R.id.bSendBroadcast)
	public void onClick(){
		String msg = etMessage.getText().toString();
		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(PHONE_NUMBER, null, msg, null, null);
	}
}

